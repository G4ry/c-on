#include "C_On.h"
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <sys/time.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#ifdef __ARM_NEON__
#define __C_ON_NEON__ 1
#endif

#if __C_ON_NEON__
#include "arm_neon.h"
#endif


using namespace cv;

using namespace std;

typedef char                                    IRTC8;
typedef unsigned char                           IRTU8;
typedef signed char                             IRTS8;
typedef unsigned short                          IRTU16;
typedef signed short                            IRTS16;
typedef unsigned int                            IRTU32;
typedef signed int                              IRTS32;
typedef float                                   IRTF32;

#ifndef IRT_DEBUG
#define IRT_DEBUG
#endif

#ifdef IRT_DEBUG
void IRTODS(const IRTC8 *DebugString,...);
#define IRTAssert(exp)  if (!(exp)) {IRTODS( "%s:%d\n",__FILE__, __LINE__ );while(1);}
#else
inline void IRTODS(const IRTC8 *DebugString,...){};
#define IRTAssert(exp)
#endif
#ifdef IRT_DEBUG
void IRTODS(const char *DebugString,...)
{
	va_list arg;
	va_start(arg, DebugString);
	static char str[1024];
	vsnprintf(str, 1024, DebugString, arg);
	va_end(arg);
    
	//slog2f(NULL, 9000, SLOG2_INFO, "%s", str);
    
	fprintf(stderr,"%s",str);
}
#endif

void M4V3PT(const float *M,const float *s,float *d,int n)
{
    for(int i=0;i<n;i++)
    {
        const float *v=s;
        float nx=s[0],ny=s[1],nz=s[2],tnx,tny,tnz;
        tnx=nx*M[0*4+0]+ny*M[1*4+0]+nz*M[2*4+0]+M[3*4+0];
        tny=nx*M[0*4+1]+ny*M[1*4+1]+nz*M[2*4+1]+M[3*4+1];
        tnz=nx*M[0*4+2]+ny*M[1*4+2]+nz*M[2*4+2]+M[3*4+2];
        tnx/=-tnz;tny/=-tnz;
        d[0]=tnx;d[1]=tny;d[2]=tnz;
        s+=3;
        d+=3;
    }
}

IRTU32 IRTMSec(void)
{
	timeval tNow;
	gettimeofday(&tNow, NULL);
	return ((IRTU32)(tNow.tv_sec*1000+tNow.tv_usec/1000));
}

double IRTUSec(void)
{
	timeval tNow;
	gettimeofday(&tNow, NULL);
	return (double(tNow.tv_sec*1000000+tNow.tv_usec));
}


string C_ONName="C-On-0";

cv::Mat C_OnImg;

void C_onCreate(C_On &C,unsigned int v)
{
    unsigned char *m;
    int w,h,s;
    C.Create(v,&m,&w,&h,&s);
    cv::Mat Img(h,w,CV_8U,m);
    C_OnImg=Img;
    imshow(C_ONName,C_OnImg);
}

cv::Mat RGBA;
const int MaxScales=4;
cv::Mat BAndW[MaxScales];
int BAndWScale;
unsigned int Counter=0;

int main(int argc, char** argv)
{
    C_On C;
    
    VideoCapture cap;
#if 1
    cap.open(0);
#else
    cap.open("http://192.168.0.8:8080//videofeed?d=m.mjpg");
#endif
    if(!cap.isOpened()) return -1;
    
    IRTU32 FrameTime=0,Frames=0,CapturedFrames=0;
    int tv=32,ctv=5;
    int lastc_on=0xfff,c_on=0,c_on_v=0,Invert=0;
    namedWindow( "Video", CV_WINDOW_AUTOSIZE );
    cv::createTrackbar("Threshold","Video",&tv,255);
    cv::createTrackbar("Contour Threshold","Video",&ctv,511);
    cv::Mat lf;
    int Filter=0;
    const int IDBits=8,IDMask=((1<<IDBits)-1);
    IRTODS("C-On G.Symons 2014\n");
    IRTODS("When focused on windows use following keys in lower case:\n");
    IRTODS("q-:Quit  f:Filter i:Invert\ns:Save C10 Image  0-9/a-f:Cycle update 32 Bit ID Of C-On image\n");
    IRTODS("Test saved C-On by displaying it from a print out or transfer to a display on a phone for instance and put in front of this application's camera.\n");
    while(1)
    {
        cv::Mat f,tf;
        const double *Rot,*Trans;
        cap >> RGBA;
        
        float sr=640.0f/RGBA.cols;
        if(sr<1) cv::resize(RGBA,RGBA,Size(640.0f,RGBA.rows*sr),0,0,INTER_AREA);
        cv::Mat grn(RGBA.rows, RGBA.cols, CV_8UC1);
        unsigned char *grnd=grn.data;
        const unsigned char *rgbas=RGBA.data+1;
        for(int j=0;j<grn.rows;j++)
        for(int i=0;i<grn.cols;i++)
        {
            *grnd++=*rgbas;
            rgbas+=3;
        }
        
        
        
        int c=waitKey(1);
        if(c=='C') Counter++;
        if(c=='q') break;
        if(c=='f') Filter^=1;
        if(c=='i') Invert^=1;
        if(c=='s') cv::imwrite(C_ONName+".jpg",C_OnImg);
        if(c=='S') cv::imwrite("BNW.jpg",tf);
        if((c>='0')&&(c<='9')) c_on=(c_on<<4)|(c-'0');
        if((c>='a')&&(c<='f')) c_on=(c_on<<4)|(c-'a'+10);
        
        c_on&=IDMask;
        
        if(lastc_on!=c_on)
        {
            unsigned int C_On=c_on;
            cv::destroyWindow(C_ONName);
            std::stringstream stream;
            stream << std::hex << c_on;
            C_ONName="C-On-0x"+stream.str();
            namedWindow( C_ONName, CV_WINDOW_AUTOSIZE );
            C_onCreate(C,C_On);
            lastc_on=c_on;
        }
        
        unsigned int ScaleMask=0xf;
        for(BAndWScale=0;BAndWScale<MaxScales;BAndWScale++)
        {
            if(BAndWScale)
            {
                BAndW[BAndWScale]=cv::Mat(BAndW[BAndWScale-1].rows/2,BAndW[BAndWScale-1].cols/2,CV_8UC1);
                C.Filter2x2(BAndW[BAndWScale-1].data,BAndW[BAndWScale-1].cols,BAndW[BAndWScale-1].rows,BAndW[BAndWScale].data,BAndW[BAndWScale-1].step,BAndW[BAndWScale].step);
            }
            else
            {
                BAndW[BAndWScale]=grn;
            }
            
            if(!(ScaleMask&(1<<BAndWScale))) continue;
            
            C_On::C_ON_M Result[256];
            int Results=C.Track(BAndW[BAndWScale].data,BAndW[BAndWScale].cols,BAndW[BAndWScale].rows,&Result[0],256,tv);
            
            for(int i=0;i<Results;i++)
            {
                const float pv[]={
                    -1,-1,0, 1,-1,0, 1,1,0, -1,1,0, -1,-1,0,
                    0,0,2, 1,-1,0, 0,0,2, 1,1,0, 0,0,2, -1,1,0,
                    0,1,0, 0,0,0, 1,0,0,
                };
                const int pvs=sizeof(pv)/(3*sizeof(float));
                static float ptv[pvs*3];
                
                M4V3PT(Result[i].Mat,pv,ptv,pvs);
                
                const float cx=RGBA.cols*0.5f,cy=RGBA.rows*0.5f,cw=cx,ch=cy;
                for(int j=0;j<pvs-1;j++)
                {
                    float *tv[2]={&ptv[j*3],&ptv[(j+1)*3]};
                    cv::line(RGBA,cv::Point2f(tv[0][0]*cw+cx,tv[0][1]*cw+cy),cv::Point2f(tv[1][0]*cw+cx,tv[1][1]*cw+cy),cv::Scalar(BAndWScale==2?255:0,BAndWScale==1?255:0,BAndWScale==0?255:0),1);
                }
                
                IRTODS("Found:%08x Frame:%2d Counter:%08x\n",Result[i].RawID,Frames,Counter);
            }
        }
        imshow("Video",RGBA);
        Frames++;
        int dt=(IRTMSec()-FrameTime);
        //if(dt>0) IRTODS("%dx%d FPS:%d %d Filter:%s\n",f.cols,f.rows,(Frames*1000)/dt,(CapturedFrames*1000)/dt,Filter?"On":"Off");
        if(!(Frames&15))
        {
            Frames=0;
            CapturedFrames=0;
            FrameTime=IRTMSec();
        }
    }
    return 0;
}

