/*
 C_On A-R-Button Copyright G.Symons 2014 All Rights Reserved.
 */
#include "C_On.h"
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <sys/time.h>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#define __C_ON_DEBUG__
namespace C_On_Space
{
    using namespace cv;
    
    using namespace std;
    
#ifdef __ARM_NEON__
#define __C_ON_NEON__ 1
#endif
    
#if __C_ON_NEON__
#include "arm_neon.h"
#endif
    
    typedef char                                    IRTC8;
    typedef unsigned char                           IRTU8;
    typedef signed char                             IRTS8;
    typedef unsigned short                          IRTU16;
    typedef signed short                            IRTS16;
    typedef unsigned int                            IRTU32;
    typedef signed int                              IRTS32;
    typedef float                                   IRTF32;
    
#ifndef IRT_DEBUG
#define IRT_DEBUG
#endif
    
#ifdef IRT_DEBUG
    void IRTODS(const IRTC8 *DebugString,...);
#define IRTAssert(exp)  if (!(exp)) {IRTODS( "%s:%d\n",__FILE__, __LINE__ );while(1);}
#else
    inline void IRTODS(const IRTC8 *DebugString,...){};
#define IRTAssert(exp)
#endif
#ifdef IRT_DEBUG
    void IRTODS(const char *DebugString,...)
    {
        va_list arg;
        va_start(arg, DebugString);
        static char str[1024];
        vsnprintf(str, 1024, DebugString, arg);
        va_end(arg);
        
        //slog2f(NULL, 9000, SLOG2_INFO, "%s", str);
        
        fprintf(stderr,"%s",str);
    }
#endif
    
    IRTF32 *M4Copy(IRTF32 *d,const IRTF32 *s)
    {
        for (IRTU32 i=0; i<16; i++) d[i]=s[i];
        return d;
    }
    
    IRTF32 *M4Mul(IRTF32 *d,const IRTF32 *a,const IRTF32 *b)
    {
        IRTF32 r[16];
        r[ 0]=a[ 0]*b[ 0]+a[ 1]*b[ 4]+a[ 2]*b[ 8]+a[ 3]*b[12];
        r[ 1]=a[ 0]*b[ 1]+a[ 1]*b[ 5]+a[ 2]*b[ 9]+a[ 3]*b[13];
        r[ 2]=a[ 0]*b[ 2]+a[ 1]*b[ 6]+a[ 2]*b[10]+a[ 3]*b[14];
        r[ 3]=a[ 0]*b[ 3]+a[ 1]*b[ 7]+a[ 2]*b[11]+a[ 3]*b[15];
        r[ 4]=a[ 4]*b[ 0]+a[ 5]*b[ 4]+a[ 6]*b[ 8]+a[ 7]*b[12];
        r[ 5]=a[ 4]*b[ 1]+a[ 5]*b[ 5]+a[ 6]*b[ 9]+a[ 7]*b[13];
        r[ 6]=a[ 4]*b[ 2]+a[ 5]*b[ 6]+a[ 6]*b[10]+a[ 7]*b[14];
        r[ 7]=a[ 4]*b[ 3]+a[ 5]*b[ 7]+a[ 6]*b[11]+a[ 7]*b[15];
        r[ 8]=a[ 8]*b[ 0]+a[ 9]*b[ 4]+a[10]*b[ 8]+a[11]*b[12];
        r[ 9]=a[ 8]*b[ 1]+a[ 9]*b[ 5]+a[10]*b[ 9]+a[11]*b[13];
        r[10]=a[ 8]*b[ 2]+a[ 9]*b[ 6]+a[10]*b[10]+a[11]*b[14];
        r[11]=a[ 8]*b[ 3]+a[ 9]*b[ 7]+a[10]*b[11]+a[11]*b[15];
        r[12]=a[12]*b[ 0]+a[13]*b[ 4]+a[14]*b[ 8]+a[15]*b[12];
        r[13]=a[12]*b[ 1]+a[13]*b[ 5]+a[14]*b[ 9]+a[15]*b[13];
        r[14]=a[12]*b[ 2]+a[13]*b[ 6]+a[14]*b[10]+a[15]*b[14];
        r[15]=a[12]*b[ 3]+a[13]*b[ 7]+a[14]*b[11]+a[15]*b[15];
        M4Copy(d,r);
        return d;
    }
    
    void M4V3PT(const float *M,const float *s,float *d,int n)
    {
        for(int i=0;i<n;i++)
        {
            const float *v=s;
            float nx=s[0],ny=s[1],nz=s[2],tnx,tny,tnz;
            tnx=nx*M[0*4+0]+ny*M[1*4+0]+nz*M[2*4+0]+M[3*4+0];
            tny=nx*M[0*4+1]+ny*M[1*4+1]+nz*M[2*4+1]+M[3*4+1];
            tnz=nx*M[0*4+2]+ny*M[1*4+2]+nz*M[2*4+2]+M[3*4+2];
            tnx/=-tnz;tny/=-tnz;
            d[0]=tnx;d[1]=tny;d[2]=tnz;
            s+=3;
            d+=3;
        }
    }
    
    float V3DP(const float *a,const float *b)
    {
        return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
    }
    
    float V3Len(const float *v)
    {
        return sqrtf(V3DP(v,v));
    }
    
    float *V3SAdd(float *d,const float *s,const float a)
    {
        d[0]=s[0]+a;
        d[1]=s[1]+a;
        d[2]=s[2]+a;
        return d;
    }
    
    float *V3SMul(float *d,const float *s,const float m)
    {
        d[0]=s[0]*m;
        d[1]=s[1]*m;
        d[2]=s[2]*m;
        return d;
    }
    
    float V3Normalize(float *n,const float *v)
    {
        float r=V3Len(v);
        V3SMul(n,v,1.0f/r);
        return r;
    }
    
    float *V3Copy(float *d,const float *s)
    {
        d[0]=s[0];
        d[1]=s[1];
        d[2]=s[2];
        return d;
    }
    
    float *V3Sub(float *d,const float *b,const float *a)
    {
        d[0]=b[0]-a[0];
        d[1]=b[1]-a[1];
        d[2]=b[2]-a[2];
        return d;
    }
    
    float *V3Add(float *d,const float *b,const float *a)
    {
        d[0]=b[0]+a[0];
        d[1]=b[1]+a[1];
        d[2]=b[2]+a[2];
        return d;
    }
    
    float *V3Cross(float *d,const float *a,const float *b)
    {
        d[0]=a[1]*b[2]-a[2]*b[1];
        d[1]=a[2]*b[0]-a[0]*b[2];
        d[2]=a[0]*b[1]-a[1]*b[0];
        return d;
    }
    
    IRTU32 IRTMSec(void)
    {
        timeval tNow;
        gettimeofday(&tNow, NULL);
        return ((IRTU32)(tNow.tv_sec*1000+tNow.tv_usec/1000));
    }
    
    double IRTUSec(void)
    {
        timeval tNow;
        gettimeofday(&tNow, NULL);
        return (double(tNow.tv_sec*1000000+tNow.tv_usec));
    }
    
    //FIRED G. Symons November 2014
#ifdef __C_ON_DEBUG__
#define FIRED_UNOPTIMIZED
#endif
    cv::Mat FIRED(cv::Mat &BW,int Threshold)
    {
        cv::Mat Result;
        Result.create(BW.rows,BW.cols,CV_8U);
        const int Offsets=8,stride=int(Result.step);
        const int Offset[Offsets]={
            -1-1*stride,-1*stride,-1*stride+1,
            -1,
            1+1*stride,1*stride,1*stride-1,
            1
        };
        
        
#if __SSE2__
        __m128i t=_mm_set1_epi8(Threshold);
        __m128i ovf=_mm_set1_epi8(31);
        __m128i covf=_mm_set1_epi8(255-31);
        for(int i=1+1*BW.step;i<((BW.step*(BW.rows-1))-1);i+=16)
        {
            __m128i c = _mm_loadu_si128((const __m128i*)(&BW.data[i]));
            c=_mm_adds_epu8(c,t);
            __m128i r=_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[0]]));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[1]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[2]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[3]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[4]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[5]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[6]])));
            r=_mm_max_epu8(r,_mm_loadu_si128((const __m128i*)(&BW.data[i+Offset[7]])));
            r=_mm_subs_epu8(r,c);
#ifdef FIRED_UNOPTIMIZED
            r=_mm_adds_epu8(r,ovf);r=_mm_and_si128(r,covf);
#endif
            _mm_storeu_si128((__m128i*)(&Result.data[i]),r);
        }
#else
#if __C_ON_NEON__
        uint8x16_t t=vdupq_n_u8(Threshold);
        uint8x16_t ovf=vdupq_n_u8(127);
        uint8x16_t covf=vdupq_n_u8(255-127);
        for(int i=1+1*BW.step;i<((BW.step*(BW.rows-1))-1);i+=16)
        {
            uint8x16_t c=vld1q_u8(&BW.data[i]);
            c=vqaddq_u8(c,t);
            uint8x16_t r=vld1q_u8(&BW.data[i+Offset[0]]);
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[1]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[2]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[3]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[4]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[5]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[6]]));
            r=vmaxq_u8(r,vld1q_u8(&BW.data[i+Offset[7]]));
            r=vqsubq_u8(r,c);
#ifdef FIRED_UNOPTIMIZED
            r=vqaddq_u8(r,ovf);r=vandq_u8(r,covf);
#endif
            vst1q_u8((uint8_t*)&Result.data[i],r);
        }
#else
        uint64_t t=(Threshold>>1),msk=0x7f7f7f7f7f7f7f7f,hs=0x8080808080808080;
        t|=t<<8;t|=t<<16;t|=t<<32;
        for(int i=1+1*BW.step;i<((BW.step*(BW.rows-1))-1);i+=8)
        {
            uint64_t r=0,c=*((uint64_t *)(&BW.data[i]));
            c>>=1;c&=msk;c+=t;
            int j=0;
            for(int j=0;j<Offsets;j++)
            {
                uint64_t o=*((uint64_t *)(&BW.data[i+Offset[j]]));
                o>>=1;o&=msk;o|=hs;o-=c;
                r|=o;
            }
            r&=hs;
            *((uint64_t *)(&Result.data[i]))=r;
        }
#endif
#endif
        return Result;
    }
    
    float Length(cv::Point2f &a,cv::Point2f &b)
    {
        cv::Point2f r=a-b;
        return sqrtf(r.x*r.x+r.y*r.y);
    }
    
    float Length(cv::Point &a,cv::Point &b)
    {
        cv::Point r=a-b;
        return sqrtf(r.x*r.x+r.y*r.y);
    }
    
    float Length2(cv::Point2f &a,cv::Point2f &b)
    {
        cv::Point2f r=a-b;
        return (r.x*r.x+r.y*r.y);
    }
    
    float Length2(cv::Point &a,cv::Point &b)
    {
        cv::Point r=a-b;
        return (r.x*r.x+r.y*r.y);
    }
    
    int CheckSum(int v)
    {
        uint64_t s=0;
        for(int j=0;j<8;j++)
        {
            s^=1;
            s=(s<<1)|(s>>3);
            s+=(v>>(j<<2));
            s&=0xf;
        }
        return s;
    }
    
    inline float Length2(const cv::Point2f &p){return (p.x*p.x+p.y*p.y);}
    inline float Length(const cv::Point2f &p){return sqrtf(Length2(p));}
    inline cv::Point2f Normalize(const cv::Point2f &p)
    {
        float l=Length(p);
        cv::Point2f r(p.x/l,p.y/l);
        return r;
    }
    
    inline float DotP(const cv::Point2f &a,const cv::Point2f &b)
    {
        return (a.x*b.x+a.y*b.y);
    }
    
    cv::Point2f VecMom(std::vector<cv::Point> &CP,cv::Point2f &Center,bool Normalized=true)
    {
        cv::Point2f o(0);
        for (int k=0;k<(int)CP.size();k++)
        {
            cv::Point2f p=CP[k];
            o=o+p;
        }
        o=o-(Center*int(CP.size()));
        if(Normalized) o=Normalize(o);
        return o;
    }
    
    cv::Point2f InsideRadius(std::vector<cv::Point> &CP,cv::Point2f &Center,float r)
    {
        cv::Point2f o(0);
        int n=0;
        r=r*r;
        for (int k=0;k<(int)CP.size();k++)
        {
            cv::Point2f p=CP[k];
            if(Length2(p,Center)<r)
            {
                o=o+p;
                n++;
            }
        }
        o=o-(Center*n);
        return o;
    }
    
    cv::Point2f ClosestTo(std::vector<cv::Point> &CP,cv::Point2f &Center)
    {
        cv::Point2f o(0);
        float mr=1e38;
        int n=0;
        for (int k=0;k<(int)CP.size();k++)
        {
            cv::Point2f p=CP[k];
            float r=Length2(p,Center);
            if(r<mr)
            {
                o=p;
                mr=r;
            }
        }
        return o;
    }
    
    int AngleDiscrete(float Angle,int Bits,int Bias=(1<<(8-1)),int BiasRangeShift=8)
    {
        int a=(Angle*(1<<(Bits+BiasRangeShift)))/(acosf(-1)*2);
        a=(a+Bias)>>BiasRangeShift;
        a&=((1<<Bits)-1);
        return a;
    }
    
    void DisplayMtx(const char *t,const cv::Mat &m)
    {
        IRTODS("%s %dx%d:\n",t,m.rows,m.cols);
        for(int i=0;i<m.rows;i++)
        {
            for(int j=0;j<m.cols;j++)
            {
                IRTODS("%f ",m.at<double>(i,j));
            }
            IRTODS("\n");
        }
    }
    
    int PAt(const cv::Mat &Img,const cv::Point2f &P0)
    {
        if(P0.x<0) return 0;
        if(P0.x>=Img.cols) return 0;
        if(P0.y<0) return 0;
        if(P0.y>=Img.rows) return 0;
        return Img.at<unsigned char>(cv::Point2f(P0.x,P0.y));
    }
    
    int DAt(const cv::Mat &Img,const cv::Point2f &P0,const cv::Point2f &P1)
    {
        int l[2]={PAt(Img,P0),PAt(Img,P1)};
        return l[0]-l[1];
    }
    
    float P2fDistance(const cv::Point2f &a,const cv::Point2f &b)
    {
        return sqrtf((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
    }
    
    double sqr(double v){return v*v;}
    
    float FarF(float a,float z){return cosf(a)/(z+sinf(a));}
    float NearF(float a,float z){return cosf(a)/(z-sinf(a));}
    float AllF(float a,float z){return NearF(a,z)+FarF(a,z);}
    float InvCAllF(float z,float l){return ((z-sqrtf(z*z-l*l*z*z+l*l))/l);}
    float InvAllF(float z,float l){return acosf(InvCAllF(z,l));}
    
    void ZFind(std::vector<cv::Point2f> &OV,float *z,double l=1,double m=1)
    {
		double a,b;
		double d=(OV[2].x-OV[1].x)*(OV[3].y-OV[1].y)-(OV[3].x-OV[1].x)*(OV[2].y-OV[1].y);
		a=((OV[1].x-OV[0].x)*(OV[2].y-OV[1].y)-(OV[2].x-OV[1].x)*(OV[1].y-OV[0].y))/d;
		b=((OV[1].x-OV[0].x)*(OV[3].y-OV[1].y)-(OV[3].x-OV[1].x)*(OV[1].y-OV[0].y))/d;
		z[0]=m/sqrt(sqr(a*OV[3].x-OV[0].x)+sqr(a*OV[3].y-OV[0].y)+sqr(l)*sqr(a-1));
		z[3]=z[0]*a;
		z[2]=z[0]*b;
		z[1]=z[2]-z[3]+z[0];
        for(int i=0;i<4;i++)
        {
            z[i]*=-l;
            OV[i].x*=-z[i];
            OV[i].y*=-z[i];
        }
    }
    
    const float C_ON_LT=7;
    const float C_ON_R[4]={40,100,108,128},C_ON_LT2=40;
    const float C_ON_R_Ratio[4]={C_ON_R[0]/C_ON_R[1],C_ON_R[0]/C_ON_R[2],C_ON_R[1]/C_ON_R[2],C_ON_R[2]/C_ON_R[3]};
    
    const int IDBits=8;
    
    void DrawEllipse(cv::Mat &Img,const cv::RotatedRect &e,cv::Scalar Col,float Thickness)
    {
        cv::Point2f ci=e.center;
        float w=e.size.width*0.5f,h=e.size.height*0.5f,ea=acosf(-1)*2*e.angle/360.0f;
        for(int i=0;i<32;i++)
        {
            float a[2]={(2*acosf(-1)*i)/32.0f,(2*acosf(-1)*((i+1)&31))/32.0f};
            cv::Point2f t[2],p[2]={cv::Point2f(w*cosf(a[0]),h*sinf(a[0])),cv::Point2f(w*cosf(a[1]),h*sinf(a[1]))};
            t[0].x=p[0].x*cosf(ea)-p[0].y*sinf(ea);t[0].y=p[0].x*sinf(ea)+p[0].y*cosf(ea);
            t[1].x=p[1].x*cosf(ea)-p[1].y*sinf(ea);t[1].y=p[1].x*sinf(ea)+p[1].y*cosf(ea);
            cv::line(Img,ci+t[0],ci+t[1],Col,Thickness);
        }
    }
    
    float DistanceFromEllipse(const cv::RotatedRect &e,cv::Point2f cp,cv::Point2f &ep)
    {
        cv::Point2f p,t,ci=e.center;
        float w=e.size.width*0.5f,h=e.size.height*0.5f,ea=acosf(-1)*2*e.angle/360.0f;
        p=cp-ci;
        t.x=p.x*cosf(-ea)-p.y*sinf(-ea);t.y=p.x*sinf(-ea)+p.y*cosf(-ea);
        float a=atan2f(t.y,t.x);
        ep=cv::Point2f(w*cosf(a),h*sinf(a));
        float l=P2fDistance(t,ep);
        t.x=ep.x*cosf(ea)-ep.y*sinf(ea);t.y=ep.x*sinf(ea)+ep.y*cosf(ea);
        ep=t+ci;
        return l;
    }
    
}

#ifdef __C_ON_DEBUG__
extern cv::Mat RGBA;
extern unsigned int Counter;
#endif

using namespace C_On_Space;

void C_On::Create(unsigned int v,unsigned char **GreyScaleImage,int *Width,int *Height,int *Stride)
{
    static cv::Mat C_OnImg(256,256,CV_8U);
    int b=IDBits;
    C_OnImg.setTo(0xff);
    cv::Point2f ci(128,128);
    
    float cr1=C_ON_R[1];
    cv::circle(C_OnImg,ci,cr1,cv::Scalar(0),-1);
    
    float cr0=C_ON_R[0];

    cv::circle(C_OnImg,ci+cv::Point2f(-cr0, cr0),C_ON_LT*3,cv::Scalar(255),-1);
    cv::circle(C_OnImg,ci+cv::Point2f( cr0, cr0),C_ON_LT*2,cv::Scalar(255),-1);
    cv::circle(C_OnImg,ci+cv::Point2f( cr0,-cr0),C_ON_LT*2,cv::Scalar(255),-1);
    cv::circle(C_OnImg,ci+cv::Point2f(-cr0,-cr0),C_ON_LT*2,cv::Scalar(255),-1);

    v^=0xffffffff;
    float cr2=C_ON_R[2],cr3=C_ON_R[3],aofs=0;
    for(int r=cr2;r<cr3;r++)
    for(int i=0;i<IDBits;i++)
    {
        if((v>>i)&1)
        {
            float a=(i*360)/IDBits,lta=(360*0.5f*0.5f)/b,lta2;
            if(((v>>((i+1)%IDBits))&1)) lta2=lta*3; else lta2=lta;
            cv::ellipse(C_OnImg,ci,cv::Size(r,r),0,aofs+a-lta,aofs+a+lta2,cv::Scalar(0,0,0),2);
        }
    }
    
    
    if(GreyScaleImage) *GreyScaleImage=C_OnImg.data;
    if(Width) *Width=C_OnImg.cols;
    if(Height) *Height=C_OnImg.rows;
    if(Stride) *Stride=C_OnImg.step;
}

int C_On::Track(unsigned char *GreyScaleImage,int Width,int Height,C_On::C_ON_M *Result,int MaxResults,unsigned int tv,unsigned int Flags)
{
    int Results=0;
    int ctv=5;
    cv::Mat bnw(Height,Width,CV_8U,GreyScaleImage);
    float cx=bnw.cols*0.5f,cy=bnw.rows*0.5f,cw=cx,ch=cy;
    
    cv::Mat f;
    
    f=FIRED(bnw,tv);
    
    std::vector<std::vector<cv::Point> > contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(f,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_NONE);
    
    const int sc=4;
    
    for(int i=0;i<(int)contours.size();i++)
    {
        if(contours[i].size()<16) continue;
        
        if(hierarchy[i][2]<0) continue;
        
        int ci[sc];
        int rh=hierarchy[i][2],n=0;
        
        while(rh>=0)
        {
            if((hierarchy[rh][3]==i)&&(contours[rh].size()>=ctv))
            {
                ci[n]=rh;
                n++;
                if(n>sc) break;
            }
            else
            break;
            rh=hierarchy[rh][0];
        }
        if(n!=sc) continue;
        
        cv::RotatedRect ee=cv::fitEllipse(contours[i]);
        
        int o[4]={0,1,2,3},lo;
        float ro[4],maxro=-1,angleo[4];
        cv::Point2f co[4],ac(0,0);
        cv::RotatedRect eo[4];
        
        for(int j=0;j<4;j++)
        {
            eo[j]=cv::fitEllipse(contours[ci[j]]);
            co[j]=eo[j].center;
            ac+=co[j];
            ro[j]=std::max(eo[j].size.width,eo[j].size.height)*0.5f;
            if(ro[j]>maxro) {maxro=ro[j];lo=j;}
        }
        ac*=0.25f;
        for(int j=0;j<4;j++)
        {
            angleo[j]=atan2f(co[j].y-ac.y,co[j].x-ac.x);
        }
        
        int ns=1;
        while(ns)
        {
            ns=0;
            for(int j=0;j<3;j++)
            {
                if(angleo[o[j]]>angleo[o[j+1]])
                {
                    int t=o[j];o[j]=o[j+1];o[j+1]=t;
                    ns=1;
                }
            }
        }

        for(int j=0;j<4;j++) if(o[j]==lo) {lo=j;break;}
        

        int olo[4]={o[lo],o[(lo+1)&3],o[(lo+2)&3],o[(lo+3)&3]};
        
        float r0=C_ON_R_Ratio[0];
        cv::Point2f t[2][4]=
        {
            {
                cv::Point2f(-r0,-r0),cv::Point2f(-r0,r0),cv::Point2f(r0,r0),cv::Point2f(r0,-r0),
            },
            {
                co[olo[0]],co[olo[1]],co[olo[2]],co[olo[3]],
            }
        };
        
        for(int j=0;j<4;j++)
        {
            t[1][j].x-=cx;t[1][j].x/=cw;
            t[1][j].y-=cy;t[1][j].y/=cw;
        }
        
        cv::Mat PT=cv::getPerspectiveTransform(t[0],t[1]);

        cv::Point2f eev[4];
        ee.points(eev);
        cv::Point2f eecc[3]={ee.center,eev[0]+(eev[2]-eev[1])*0.5f,eev[1]+(eev[3]-eev[2])*0.5f};
        for(int j=0;j<3;j++)
        {
            eecc[j].x-=cx;eecc[j].x/=cw;
            eecc[j].y-=cy;eecc[j].y/=cw;
        }
        std::vector<cv::Point2f> epvt,eptv;
        epvt.assign(eecc,eecc+3);
        cv::perspectiveTransform(epvt,eptv,PT.inv());
        
        if((Length(eptv[1])<0.9f)||(Length(eptv[1])>1.1f)||(Length(eptv[2])<0.9f)||(Length(eptv[2])>1.1f))
        {
            //IRTODS("Ellipse doesn't fit:%f,%f(%f) %f,%f(%f)\n",eptv[1].x,eptv[1].y,Length(eptv[1]),eptv[2].x,eptv[2].y,Length(eptv[2]));
            continue;
        }
        if(Length(eptv[0])>0.3f)
        {
            //IRTODS("Center doesn't fit:%f,%f(%f)\n",eptv[0].x,eptv[0].y,Length(eptv[0]));
            continue;
        }
        
#ifdef __C_ON_DEBUG__
        float ldt=3;
        
        DrawEllipse(RGBA,ee,cv::Scalar(0,255,0),ldt);
        
        cv::line(RGBA,t[1][0],t[1][1],cv::Scalar(0,255,0),ldt);
        cv::line(RGBA,t[1][1],t[1][2],cv::Scalar(0,255,0),ldt);
        cv::line(RGBA,t[1][2],t[1][3],cv::Scalar(0,255,0),ldt);
        cv::line(RGBA,t[1][3],t[1][0],cv::Scalar(0,255,0),ldt);
        for(int j=0;j<4;j++)
        {
            cv::circle(RGBA,co[olo[j]],ro[olo[j]],cv::Scalar(((j>>1)&1)*255,255,(j&1)*255),-1);
        }
#endif
        
        std::vector<cv::Point2f> ivt,itv;
        float r=(C_ON_R[3]+C_ON_R[2])/(2*C_ON_R[1]);
        for(int j=0;j<IDBits;j++)
        {
            float a=(-j*acosf(-1)*2)/IDBits;
            ivt.push_back(cv::Point2f(r*cosf(a),r*sinf(a)));
        }
        
        cv::perspectiveTransform(ivt,itv,PT);
        for(int j=0;j<IDBits;j++)
        {
            itv[j].x=itv[j].x*cw+cx;
            itv[j].y=itv[j].y*cw+cy;
        }
        
        int rt=0,ct=0,cf=1;
        
        for(int j=0;j<IDBits;j++)
        {
#ifdef __C_ON_DEBUG__
            cv::circle(RGBA,itv[j],2,cv::Scalar(0,255-(j*128)/IDBits,255),1);
#endif
            int b=DAt(bnw,itv[j],itv[(j+IDBits-1)%IDBits]);
            int ab=(abs(b)>=tv);
            ct^=ab;
            if(cf&&ab)
            {
                if((b>0)^ct)
                {
                    ct^=1;
                    rt^=((1<<j)-1);
                }
                cf=0;
            }
            rt|=(ct<<j);
        }
        Result[Results].RawID=rt;
        
        cv::Point2f qv[4]={cv::Point2f(0,0),cv::Point2f(0,1),cv::Point2f(1,1),cv::Point2f(1,0)};
        ivt.clear();
        ivt.assign(qv,qv+4);
        cv::perspectiveTransform(ivt,itv,PT);
        
        float qz[4];
        ZFind(itv,qz);

        float M4x4[16]=
        {
            itv[3].x-itv[0].x,itv[3].y-itv[0].y,qz[3]-qz[0],0,
            itv[1].x-itv[0].x,itv[1].y-itv[0].y,qz[1]-qz[0],0,
            0,0,1,0,
            itv[0].x,itv[0].y,qz[0],1,
        };
        
        V3Cross(&M4x4[8],&M4x4[4],&M4x4[0]);
        
        M4Copy(Result[Results].Mat,M4x4);
        Results++;
        if(Results>=MaxResults) break;

    }
    return Results;
}

void C_On::Filter2x2(const unsigned char *SrcGreyScale,int Width,int Height,unsigned char *DstGreyScale,int SrcStride,int DstStride)
{
    Height&=~1;
    uint64 *s=(uint64 *)SrcGreyScale;
    uint8_t *d=(uint8_t *)DstGreyScale;
    int w64=Width>>3,ss64=((SrcStride>>3)<<1),sd=DstStride;
    if(!ss64) ss64=(w64<<1);
    if(sd) sd-=(Width>>1);
    uint64 m;
    uint8_t *p=(uint8_t *)&m;
    for(int j=0;j<Height;j+=2)
    {
        for(int i=0;i<w64;i++)
        {
            m=((s[i]>>2)&0x3f3f3f3f3f3f3f3f)+((s[i+w64]>>2)&0x3f3f3f3f3f3f3f3f);
            m+=(m>>8);
            *d++=p[0];*d++=p[2];*d++=p[4];*d++=p[6];
        }
        d+=sd;
        s+=ss64;
    }
}

int  C_On::HammingDistance(unsigned int A,unsigned int B)
{
    return cv::normHamming((unsigned char *)&A,(unsigned char *)&B,4);
}

