#ifndef _C_ON_H_
#define _C_ON_H_
/*
 C_On A-R-Button Copyright G.Symons 2014 All Rights Reserved.
 */
class C_On
{
    public:
    struct C_ON_M
    {
        float Mat[4*4];
        unsigned int RawID;
    };
    
    void Create(unsigned int Value,unsigned char **GreyScaleImage,int *Width,int *Height,int *Stride);
    int  Track(unsigned char *GreyScaleImage,int Width,int Height,C_ON_M *C_OnResult,int MaxResults,unsigned int ThresholdValue,unsigned int Reserved=0);
    void Filter2x2(const unsigned char *SrcGreyScale,int Width,int Height,unsigned char *DstGreyScale,int SrcStride,int DstStride);
    int  HammingDistance(unsigned int A,unsigned int B);
};
#endif

